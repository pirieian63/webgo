/*********************************************************************
 * Name: Ian Pirie
 * Date: 12/12/2018
 * File: helper.h
 * 
 * Description: This holds a namespace to hold various different 
 * parts of the application that are necessary. Hopefully will help
 * streamline sharing of constants down the road as well.
 * ******************************************************************/
#include <string>


/// Helper namespace
namespace helper{

    /// Holds the workspace
    std::string workspace;

    const std::string type[4] = {"","javascript","php","jsp"};

    int id;

    /**
     * 1: Flat, every function gets its own file.
     * 2: Bulk, all functions go into the same file.
     * 3: Each individual file has a seperate file.
     * */
    int save_type;

}