/*********************************************************************
 * Name: Ian Pirie
 * Date: 12/12/2018
 * File: index.cpp
 * 
 * Description: This hold the constructors for the index header file.
 * ******************************************************************/

#include "index.h"
using std::cerr;


index::index()
{

    files.push_back("index.txt");
    std::ofstream os("index.html",std::ostream::app);
    os  << "main.html";
    os.flush();
    os.close();
    files.push_back("main.html");
    os.open("main.html",std::ostream::app);
    os  << "<!DOCTYPE html>\n"
        << "<html>\n"
        << "</html>";
    os.flush();
    os.close();
}

index::index(std::string ind)
{
    std::ifstream os;
    std::string temp;
    try{
        os.open(ind);

        if(os.is_open())
        {
            
        }
        else{
            throw(ind + " is not found");
        }
    }
    catch(std::string e)
    {
        cerr << e;
    }
    while(!os.eof())
    {
        std::getline(os, temp);

    }
}

