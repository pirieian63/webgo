/*********************************************************************
 * Name: Ian Pirie
 * Date: 12/12/2018
 * File: index-1.cpp
 * 
 * Description: This hold the various other function for index.h.
 * ******************************************************************/
#include "index.h"

void index::insertIndex(std::string str)
{
    int e = files.size();
    for(int i = 0; i < e; ++i)
    {
        if(files[i] == str)
        {
            std::cout << "\"" << str << "\" is already in the index.\n";
            return;
        }     
    }
    std::ofstream os("index.txt",std::ofstream::app);
    os << "\n" << str;
    files.push_back(str);
}

void index::insertIndex(std::string str, std::ostream& os)
{
    int e = files.size();
    for(int i = 0; i < e; ++i)
    {
        if(files.at(i) == str)
        {
            os << "\"" << str << "\" is already in the index.\n";
            return;
        }     
    }
    files.emplace_back(str);
}

void index::removeIndex(std::string str)
{
    auto e = files.size();
    for(int i = 0; i < e; ++i)
    {
        if(files.at(i) == str)
        {
            files.erase(files.begin()+i);
            return;
        }
    }
}