# WebGo

This Application is to make it stupid simple to program with languages.

It will auto generate function based on visual input. Example: You 
create a radio button, it autogenerates a function based on the 
language you desire said function to be in and include the control
structure like a switch statement so you don't have to fiddle with 
setup up steps you should not have to deal with.

Initially, it will be for html and internet based languages like 
javascript and php.

Eventually, it will include qt and cpp hopefully if I get around to it.

If you wish to help, email me at pirieian63@gmail.com